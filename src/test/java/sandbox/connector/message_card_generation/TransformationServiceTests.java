package sandbox.connector.message_card_generation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@Import(Config.class)
public class TransformationServiceTests {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    TransformationService transformationService;

    @Test
    public void samplePushShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_push_event.json");
    }

    @Test
    public void sampleMergeShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_merge_request_event.json");
        shouldTransformToValidJson("/sample_merge_request_no_assignee_event.json");
    }

    @Test
    public void sampleCommitCommentShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_commit_comment_event.json");
    }

    @Test
    public void sampleMergeRequestCommentShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_merge_request_comment_event.json");
    }

    @Test
    public void sampleWikiUpdateShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_wiki_page_event.json");
    }

    @Test
    public void samplePipelineShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_pipeline_event.json");
    }

    @Test
    public void sampleBuildShouldTransform() throws IOException {
        shouldTransformToValidJson("/sample_build_event.json");
    }

    private void shouldTransformToValidJson(String sampleEvent) throws IOException {
        JsonNode jsonNode = objectMapper.readTree(get(sampleEvent));
        String r = transformationService.gitlabJsonToMsTeamsJson(jsonNode.toString());
        System.out.println(r);
        assertFalse(r.contains("$"));//this also assures that template exists for the sample. Ugly, but I'm lazy
        assertTrue(isValidJSON(r));
        //todo: isValidMessageCard(r)
    }

    private boolean isValidJSON(final String json) throws IOException {
        try {
            objectMapper.readTree(json);
            return true;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
    }

    private URL get(String name) {
        return getClass().getResource(name);
    }

    @TestConfiguration
    static class Config {
        @Bean
        ObjectMapper jackson() {
            return new ObjectMapper();
        }
    }

}
