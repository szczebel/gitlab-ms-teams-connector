package sandbox.connector.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import sandbox.connector.JsonSender;
import sandbox.connector.message_card_generation.TransformationService;
import sandbox.connector.persistence.entity.Connection;
import sandbox.connector.persistence.repo.ConnectionRepo;

import java.io.IOException;
import java.util.Optional;

@RestController
public class Connector {

    public static final String POST_GITLAB_EVENT_ENDPOINT = "postGitlabEvent";
    @Autowired
    ConnectionRepo connectionRepo;
    @Autowired
    TransformationService transformationService;
    @Autowired
    JsonSender sender;


    @PostMapping(path = "/" + POST_GITLAB_EVENT_ENDPOINT, consumes = "application/json")
    public void postEvent(@RequestHeader("X-Gitlab-Token") String secret, @RequestBody String gitlabEvent) throws IOException {
        Optional<Connection> connection = connectionRepo.findById(secret);
        if (!connection.isPresent()) {
            System.out.println("No connection defined for token " + secret);
            return;
        }
        //dump input directly to sysout without logger, so tat I can copy it from console easily for review
        System.out.println(gitlabEvent);
        String msTeamsJson = transformationService.gitlabJsonToMsTeamsJson(gitlabEvent);
        String address = connection.get().getMsTeamsChannelUrl();
        sender.send(address, msTeamsJson);
    }
}
