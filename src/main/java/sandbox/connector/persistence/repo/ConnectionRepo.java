package sandbox.connector.persistence.repo;

import org.springframework.data.repository.CrudRepository;
import sandbox.connector.persistence.entity.Connection;

import java.util.Collection;

public interface ConnectionRepo extends CrudRepository<Connection, String> {

    Collection<Connection> findByOwner(String owner);
}
