package sandbox.connector.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Connection {

    @Id
    private String secret;
    @Column private String owner;
    @Column private String connectionName;
    @Column private String msTeamsChannelUrl;

    public Connection() {
    }

    public Connection(String secret, String owner, String connectionName, String msTeamsChannelUrl) {
        this.secret = secret;
        this.owner = owner;
        this.connectionName = connectionName;
        this.msTeamsChannelUrl = msTeamsChannelUrl;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public String getMsTeamsChannelUrl() {
        return msTeamsChannelUrl;
    }

    public void setMsTeamsChannelUrl(String msTeamsChannelUrl) {
        this.msTeamsChannelUrl = msTeamsChannelUrl;
    }

    public static Connection create(String name) {
        Connection connection = new Connection();
        connection.setConnectionName(name);
        return connection;
    }

    public Connection secret(String s) {
        this.secret = s;
        return this;
    }

    public Connection owner(String s) {
        this.owner = s;
        return this;
    }

    public Connection url(String s) {
        this.msTeamsChannelUrl = s;
        return this;
    }

}
