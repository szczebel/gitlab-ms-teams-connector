package sandbox.connector.security;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static sandbox.connector.rest.Connector.POST_GITLAB_EVENT_ENDPOINT;

@EnableOAuth2Sso
@Configuration
@Order(99)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.POST, "/"+ POST_GITLAB_EVENT_ENDPOINT)
        ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.oauth2Login().and()
                .authorizeRequests()
                .antMatchers("/", "/error","/oauth2/**").permitAll()
                .anyRequest().authenticated();
    }
}

//todo: add tests
