package sandbox.connector.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import sandbox.connector.JsonSender;
import sandbox.connector.message_card_generation.MessageCardGenerator;
import sandbox.connector.persistence.repo.ConnectionRepo;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static sandbox.connector.persistence.entity.Connection.create;
import static sandbox.connector.rest.Connector.POST_GITLAB_EVENT_ENDPOINT;

@Controller
public class Gui {

    private static final String CREATE_CONNECTOR = "/createConnector";
    @Autowired
    ConnectionRepo connectionRepo;
    @Autowired
    MessageCardGenerator messageCardGenerator;
    @Autowired
    JsonSender sender;

    @GetMapping("/")
    ModelAndView home(OAuth2AuthenticationToken principal, HttpServletRequest req) {
        Map<String, Object> model = new HashMap<>();
        model.put("postGitlabEventUrl", req.getRequestURL() + POST_GITLAB_EVENT_ENDPOINT);
        if (principal != null) {
            model.put("connectors", connectionRepo.findByOwner(nameOf(principal)));
        }
        return new ModelAndView("index", model);
    }

    @PostMapping(CREATE_CONNECTOR)
    public String createConnector(OAuth2AuthenticationToken user, HttpServletRequest req, @RequestParam String connectionName, @RequestParam String msTeamsChannelUrl) throws IOException {
        String username = nameOf(user);
        String secretToken = UUID.randomUUID().toString();
        sendWelcomeMessage(msTeamsChannelUrl, username, getHomeUrl(req), secretToken);
        connectionRepo.save(
                create(connectionName)
                        .secret(secretToken)
                        .owner(username)
                        .url(msTeamsChannelUrl));
        return "redirect:/";
    }

    private void sendWelcomeMessage(String msTeamsChannelUrl, String username, String homeUrl, String secretToken) {
        Map<String, Object> model = new HashMap<>();
        model.put("username", username);
        model.put("home_url", homeUrl);
        model.put("secret", secretToken);
        String message = messageCardGenerator.generateFromTemplate("connector_welcome", model);
        sender.send(msTeamsChannelUrl, message);
    }

    private String getHomeUrl(HttpServletRequest req) {
        String reqUrl = req.getRequestURL().toString();
        //CONNECTOR = getContextPath + getQueryString?
        return reqUrl.substring(0, reqUrl.length()-CREATE_CONNECTOR.length());
    }

    private String nameOf(OAuth2AuthenticationToken token) {
        //warning: non-Google auth providers might not have this field
//        return token.getPrincipal().getAttributes().get("email").toString();
        return token.getPrincipal().getName();
    }
}
