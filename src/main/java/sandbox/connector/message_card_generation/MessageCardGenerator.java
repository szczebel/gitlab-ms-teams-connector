package sandbox.connector.message_card_generation;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;

import java.util.Map;

public class MessageCardGenerator {

    private final TemplateEngine messageCardEngine;

    public MessageCardGenerator(TemplateEngine messageCardEngine) {
        this.messageCardEngine = messageCardEngine;
    }

    public String generateFromTemplate(String templateName, Map<String, Object> model) {
        IContext context = new Context(null, model);
        return messageCardEngine.process(templateName, context);
    }

}
