package sandbox.connector.message_card_generation;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class TransformationService {

    private final MessageCardGenerator messageCardGenerator;
    private final ObjectMapper jackson;

    TransformationService(MessageCardGenerator messageCardGenerator, ObjectMapper jackson) {
        this.messageCardGenerator = messageCardGenerator;
        this.jackson = jackson;
    }

    public String gitlabJsonToMsTeamsJson(String gitlabEvent) throws IOException {
        Map jsonAsMap = jackson.readValue(gitlabEvent, Map.class);
        String eventType = String.valueOf(jsonAsMap.get("object_kind"));
        return messageCardGenerator
                .generateFromTemplate(overrideIfUnknown(eventType), jsonAsMap);
    }


    private String overrideIfUnknown(String templateName) {
        //thymeleaf sucks, there should be API in ITemplateResolver to do this
        URL resource = getClass().getResource(Config.TEMPLATES_PREFIX + templateName + Config.TEMPLATES_SUFFIX);
        return resource != null ? templateName : "unknown";
    }
}
