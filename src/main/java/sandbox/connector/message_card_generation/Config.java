package sandbox.connector.message_card_generation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.dialect.SpringStandardDialect;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;


@Configuration
public class Config {

    static final String TEMPLATES_PREFIX = "/message_card_templates/";
    static final String TEMPLATES_SUFFIX = ".json";

    @Bean
    TemplateEngine messageCardEngine() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix(TEMPLATES_PREFIX);
        templateResolver.setSuffix(TEMPLATES_SUFFIX);
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setCharacterEncoding("UTF8");
        TemplateEngine thymeleaf = new TemplateEngine();
        thymeleaf.setDialect(new SpringStandardDialect());
        thymeleaf.addTemplateResolver(templateResolver);
        return thymeleaf;
    }

    @Bean
    MessageCardGenerator messageCardGenerator(TemplateEngine messageCardEngine) {
        return new MessageCardGenerator(messageCardEngine);
    }

    @Bean
    TransformationService transformationService(MessageCardGenerator messageCardGenerator, ObjectMapper jackson) {
        return new TransformationService(messageCardGenerator, jackson);
    }

}