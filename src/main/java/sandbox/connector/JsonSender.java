package sandbox.connector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class JsonSender {

    @Autowired
    RestTemplateBuilder rt;

    public void send(String address, String payload){
        rt.build().postForEntity(address, prepareRequest(payload), Void.class);
    }

    private HttpEntity<String> prepareRequest(String payload) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(payload, headers);
    }
}
